# Purpose

This repository contains ALM driver call sequence.  


# Style guide
* Use 4 spaces to created an indented sub-list.
* Add call to a subroutine as a new item of an unorderded list.
* To list call sequence of a subroutine that is outside `clm_driver.F90`, 
  add the file-name as the first item of a new sub-list
  via "`!  =========== FILE-NAME.F90  =========== ` ".
* To indicate coupling calls between ALM and external model, 
  create a new sub-list with the first item as "`!  =========== L-E_<EMODEL>  =========== ` ". 
  Note `EMODEL` will be unique for each external model.
