`subroutine clm_drv()`


* `if (use_cn)`
    * `if ( n_drydep > 0 .and. drydep_method == DD_XLND ) call interpMonthlyVeg() `
* `else`
    * `if (doalb .or. ( n_drydep > 0 .and. drydep_method == DD_XLND )) call interpMonthlyVeg() `
* `endif`
* `do: nclumps`
    * `call alt_calc() `
    * `if (use_cn) call decomp_vertprofiles() `
* `do: nclumps`
    * `if (use_betr .and. (.not. do_betr_leaching))  call begin_betr_tracer_massbalance() ` 
    * `if (use_cn)`
        * `call BeginCBalance() `
        * `call BeginNBalance() `
        * `call BeginPBalance() `
        * `call carbonflux_vars%ZeroDWT() `
        * `if (use_c13) call c13_carbonflux_vars%ZeroDWT() `
        * `if (use_c14) call c14_carbonflux_vars%ZeroDWT() `
        * `call nitrogenflux_vars%ZeroDWT() `
        * `call phosphorousflux_vars%ZeroDWT() `
        * `call carbonstate_vars%ZeroDWT() `
        * `call nitrogenstate_vars%ZeroDWT() `
        * `call phosphorousstate_vars%ZeroDWT() `
    * `endif`
* `enddo! nclumps`
* `call dynSubgrid_driver() `
    * `if (use_ed) call dyn_ED() `
* `do: nclumps`
    * `call BeginWaterBalance() `
* `enddo! nclumps`
* `if (use_cn)`
    * `call ndep_interp() `
    * `call CNFireInterp() `
* `endif`
* `if (use_cn)`
    * `call pdep_interp() `
* `endif`
* `do: nclumps`
    * `call UpdateDaylength() `
    * `call clm_drv_init() `
    * `call downscale_forcings() `
    * `call CanopyHydrology() `
    * `! =========== changeroup A: L-E_FATES (proposed) ===========`
    * `if (use_ed) `
        * `call clm_fates%wrap_sunfrac() `
    * `else `
        * `call CanopySunShadeFracs()`
    * `endif `
    * `call SurfaceRadiation() `! <--MODIFIED CONTENTS OF CANOPYSUNSHADEFRACS() REMOVED
    * `! =========== changegroup A: ALM V1 (existing) ===========`
    * `call SurfaceRadiation() `
    * `! =========== changegroup A =========== `
    * `call UrbanRadiation() `
    * `call CanopyTemperature() `
    * `call waterflux_vars%Reset() `
    * `call BareGroundFluxes() `
    * `call CanopyFluxes() `
        * `! =========== changeroup B: L-E_FATES (proposed) =========== `
        * `call clm_fates%prep_canopyfluxes() `
        * `call clm_fates%wrap_btran() `
        * `call clm_fates%wrap_photosynthesis() ! <-- INSIDE OF STABILITY ITERATION LOOP `
        * `call clm_fates%wrap_accumulate_fluxes() `
        * `! =========== changegroup B =========== `
    * `call UrbanFluxes() `
    * `call LakeFluxes() `
    * `call DustEmission() `
    * `call DustDryDep() `
    * `if (use_voc) call VOCEmission() `
    * `call LakeTemperature() `
    * `call SoilTemperature() `
    * `call SoilFluxes() `
    * `call clm_drv_patch2col(waterstate_vars, energyflux_vars, waterflux_vars)`
    * `call HydrologyNoDrainage() `
        * `! =========== HydrologyNoDrainageMod.F90 =========== `
        * `call BuildSnowFilter()`
        * `call SnowWater()`
            * `! =========== SnowHydrologyMod.F90 =========== `
            * `MODIFIED for use_vsfm`
        * `if (use_vichydro) call CLMVICMap()`
        * `call SurfaceRunoff()`
        * `call Infiltration()`
        * `if (use_betr)  call pre_diagnose_soilcol_water_flux()`
        * `if (use_vsfm ) call DrainageVSFM()`
        * `call SoilWater()`
            * `! =========== SoilWaterMovementMod.F90 =========== `
            * `select case(soilroot_water_method) `
                * `case (zengdecker_2009)`
                    * `call soilwater_zengdecker2009()`
                * `case (vsfm)`
                    * `call soilwater_vsfm()`
            * `end select`
            * `if (use_betr) then`
                * `Do some fixes to avoid negative liquid water`
            * `endif`
        * `if (use_betr)  then`
            * `call diagnose_advect_water_flux()`
            * `call calc_smp_l()`
        * `endif (use_betr)  then`
        * `if (use_vichydro) call CLMVICMap()`
        * `call WaterTable()`
            * `! =========== SoilHydrologyMod.F90 =========== `
            * `MODIFIED for use_vsfm`
        * `if (use_betr) call calc_dew_sub_flux()`
        * `call SnowCompaction()`
        * `call CombineSnowLayers()`
        * `call DivideSnowLayers()`
        * `call BuildSnowFilter()`
    * `call AerosolMasses() `
    * `call LakeHydrology() `
    * `call AerosolMasses() `
    * `call SnowAge_grain() `
    * `if (is_active_betr_bgc) `
        * `call CNEcosystemDynBetrVeg() `
        * `call run_betr_one_step_without_drainage() `
        * `call CNEcosystemDynBetrSummary() `
    * `else `
        * `if (.not. use_ed) ` 
            * `if (use_cn) `
                * `call CNEcosystemDynNoLeaching1() `
                    * `! =========== CNEcosystemDynMod.F90 =========== `
                    * `call carbonflux_vars%SetValues() `
                    * `if ( use_c13 ) call c13_carbonflux_vars%SetValues() `
                    * `if ( use_c14 ) call c14_carbonflux_vars%SetValues() `
                    * `call nitrogenflux_vars%SetValues() `
                    * `call phosphorousflux_vars%SetValues() `
                    * `call CNNDeposition() `
                    * `if (.not. nu_com_nfix)`
                        * `call CNNFixation() `
                    * `else`
                        * `call CNNFixation_QZ() `
                    * `endif`
                    * `if (crop_prog)` 
                        * `call CNNFert() `
                        * `call CNSoyfix() `
                    * `endif`
                    * `call CNMResp() `
                    * `if (nu_com .ne. 'RD')`
                        * `call PWeathering() `
                        * `call PAdsorption() `
                        * `call PDesorption() `
                        * `call POcclusion() `
                        * `if (.not. nu_com_phosphatase)`
                            * `call PBiochemMin() `
                        * `else`
                            * `call PBiochemMin_QZ() `
                        * `endif`
                    * `endif`
                    * `call PDeposition() `
                    * `call decomp_vertprofiles() `
                    * `call CNAllocation1_PlantNPDemand_Shared_BigLeaf() `
                    * `if (nu_com .eq. 'RD') call CNAllocation1_PlantNPDemand_RD() `
                * `if (use_bgc_interface)`
                    * `call get_clm_bgc_data() `
                    * `if (use_pflotran .and. pf_cmode)`
                        * `call clm_pf_run () `
                        * `call update_bgc_data_pf2clm () `
                    * `elseif (use_clm_bgc)`
                        * `call clm_bgc_run () `
                        * `call update_bgc_data_clm2clm () `
                    * `endif`
                * `endif ! if (use_bgc_interface)`
                * ` call CNEcosystemDynNoLeaching2() `
                    * `! =========== CNEcosystemDynMod.F90 =========== `
                    * `if (.not.use_ed)`
                        * `if (.not.use_bgc_interface)`
                            *  `call CNDecompAlloc() `
                                *  `! =========== CNDecompMod.F90 =========== `
                                * `if (use_century_decomp) ! Moved down from CNEcosystemDynNoLeaching() `
                                    * `call decomp_rate_constants_bgc() `
                                  * `else`
                                    * `call decomp_rate_constants_cn() `
                                * `endif`
                                * `cn_decomp_pools (c,j,l) =`
                                * `p_decomp_cpool_loss(c,j,k) = `
                                * `pmnf_decomp_cascade(c,j,k) `
                                * `immob(c,j) = `
                                * `gross_nmin_vr(c,j) = `
                                * `potential_immob_vr(c,j) = `
                                * `phr_vr(c,j) = `
                                * `if (use_nitrif_denitrif) call nitrif_denitrif() `
                                * `call CNAllocation2_ResolveNPLimit() `
                                * `p_decomp_cpool_loss(c,j,k) = `
                                * `pmnf_decomp_cascade(c,j,k) = `
                                * `sminn_to_denit_decomp_cascade_vr(c,j,k) = `
                                * `decomp_cascade_hr_vr(c,j,k) = `
                                * `decomp_cascade_ctransfer_vr(c,j,k) = `
                                * `decomp_cascade_ntransfer_vr(c,j,k) = `
                                * `decomp_cascade_sminn_flux_vr(c,j,k) = `
                                * `net_nmin_vr(c,j) = `
                                * `hrsum(c,j) = `
                                * `fphr(c,j) = `
                                * `net_nmin(c) = `
                                * `gross_nmin(c) = `
                        * `endif ! if (.not.use_bgc_interface)`
                        * `call CNDecompAlloc2() `
                            * `! =========== CNDecompMod.F90 =========== `
                            * `call CNAllocation3_PlantCNPAlloc_compute_fs(f1, f2, f3)`
                            * `call CNAllocation3_PlantCNPAlloc(f1, f2, f3)`
                        * `call CNPhenology() `
                        * `call CNGResp() `
                        * `if (use_dynroot) call CNRootDyn() `
                        * `call CStateUpdate0() `
                        * `if (use_c13) call CStateUpdate0() `
                        * `if (use_c14) call CStateUpdate0() `
                        * `if ( use_c13 ) call CIsoFlux1() `
                        * `if ( use_c14 ) call CIsoFlux1() `
                        * `call CStateUpdate1() `
                        * `if (use_c13) call CStateUpdate1() `
                        * `if (use_c14) call CStateUpdate1() `
                        * `call CNSoilLittVertTransp() `
                        * `call CNGapMortality() `
                        * `if ( use_c13 ) call CIsoFlux2() `
                        * `if ( use_c14 ) call CIsoFlux2() `
                        * `call CStateUpdate2() `
                        * `if (use_c13) call CStateUpdate2() `
                        * `if (use_c14) call CStateUpdate2() `
                        * `call NStateUpdate2() `
                        * `if (flanduse_timeseries/=' ') call CNHarvest() `
                        * `call CStateUpdate2h() `
                        * `if ( use_c13 ) call CStateUpdate2h() `
                        * `if ( use_c14 ) call CStateUpdate2h() `
                        * `call NStateUpdate2h() `
                        * `call CNWoodProducts() `
                        * `call CNFireArea() `
                        * `call CNFireFluxes() `
                        * `if ( use_c13 ) call CIsoFlux3() `
                        * `if ( use_c14 ) call CIsoFlux3() `
                        * `call CStateUpdate3() `
                        * `if (use_c13) call CStateUpdate3() `
                        * `if (use_c14) call CStateUpdate3() `
                        * `if (use_c14) call C14Decay() `
                        * `if (use_c14) call C14BombSpike() `
                        * `if (nu_com .ne. 'RD') call update_plant_stoichiometry() `
                    * `endif ! if (.not.use_ed)`
                * `call CNAnnualUpdate() `
            * `else ! (if use_cn) `
                * `if (doalb) call SatellitePhenology () `
            * `endif`
        * `else ! ED`
            * `! =========== changeroup C: L-E_FATES (proposed) =========== `
            * `! THIS PART OF THE CALL SEQUENCE NEEDS EVALUATION (RGK)` 
            * `! =========== changegroup C =========== `
            * `call carbonflux_vars%SetValues() `
            * `call c13_carbonflux_vars%SetValues() `
            * `call c14_carbonflux_vars%SetValues() `
            * `call nitrogenflux_vars%SetValues() `
        * `endif ! if (.not.ed)`
    * `endif ! if (is_active_betr_bgc)`
    * `call depvel_compute() `
    * `if (use_betr)`
        * `if (do_betr_leaching)`
            * `call bgc_reaction%init_betr_alm_bgc_coupler() `
            * `call begin_betr_tracer_massbalance() `
        * `endif`
        * `call call run_betr_one_step_without_drainage() `
    * `endif`
    * `if (use_lch4) call ch4() `
    * `call HydrologyDrainage() `
        * `! =========== HydrologyDrainageMod.F90 =========== `
        * `if (use_vichydro) call CLMVICMap() `
        * `if (use_betr) call pre_diagnose_soilcol_water_flux () `
        * `if (.not. use_vsfm) call Drainage() `
            * `! =========== SoilHydrologyMod.F90 =========== `
            * `MODIFIED for use_vsfm`
        * `if (use_betr) call diagnose_drainage_water_flux () `
    * `if (use_betr)`
        * `call run_betr_one_step_with_drainage() `
        * `call betr_tracer_massbalance_check() `
        * `call bgc_reaction%betr_alm_flux_statevar_feedback() `
    * `endif`
    * `! =========== changeroup D: L-E_FATES (proposed) =========== `
    * `if (use_ed .and. is_beg_curr_day()) `
        * `call clm_fates%dynamics_driv() `
            * `call ed_ecosystem_dynamics() ! FATES native routine `
            * `call ed_update_site() ! FATES native routine `
            * `call clm_fates%wrap_litter_fluxout() `
            * `call clm_fates%wrap_update_hlmfates_dyn() `
            * `call clm_fates%fates_hio%update_history_dyn() `
        * `call setFilters() !Supposedly unnecessary `
    * `if (use_ed) `
        * `call EDBGCDyn()`
        * `call EDBGCDynSummary(...,clm_fates, nc) `
            * `call clm_fates%wrap_bgc_summary() `
    * `! =========== changegroup D =========== `
    
    * `if (.not. use_ed)`
        * `if (use_cn)`
            * `if (is_active_betr_bgc) `
                * ` call CNFluxStateBetrSummary() `
            * `else `
                * `call CNEcosystemDynLeaching() `
                    * `! =========== CNEcosystemDynMod.F90 =========== `
                    * `call CNNLeaching() `
                    * `call NStateUpdate3() `
                    * `call CNPrecisionControl() `
                    * `call carbonflux_vars%Summary() `
                    * `call carbonstate_vars%Summary() `
                    * `if (use_c13) call carbonflux_vars%Summary() `
                    * `if (use_c13) call carbonstate_vars%Summary() `
                    * `if (use_c14) call carbonflux_vars%Summary() `
                    * `if (use_c14) call carbonstate_vars%Summary() `
                    * `call nitrogenflux_vars%Summary() `
                    * `call nitrogenstate_vars%Summary() `
            * `endif ! if (is_active_betr_bgc) `
            * `if (doalb) call CNVegStructUpdate() `
        * `endif ! (use_cn) `
    * `endif ! if (.not. use_ed)`
    * `call BalanceCheck() `
    * `if (.not. use_ed) `
        * `if (use_cn) `
            * `call CBalanceCheck() `
            * `call NBalanceCheck() `
            * `call PBalanceCheck() `
        * `endif ! if (use_cn) `
    * `endif ! if(.not. use_ed)`
    * `if (doalb) `
        * `! =========== changegroup F: ALM V1 (existing) ===========`
        * `call SurfaceAlbedo() `
        * `! =========== changeroup F: L-E_FATES (proposed) =========== `
        * `call SurfaceAlbedo(...,clm_fates, ...) `
            * `call clm_fates%wrap_canopy_radiation() `
        * `! =========== changegroup F =========== `
        * `if (doalb) call UrbanAlbedo () `
    * `endif `
* `end do ! num_clumps`
* `call lnd2atm() `
* `if (create_glacier_mec_landunit) call lnd2glc_vars%update_lnd2glc() `
* `call write_diagnostic() `
* `call atm2lnd_vars%UpdateAccVars() `
* `call temperature_vars%UpdateAccVars() `
* `call canopystate_vars%UpdateAccVars() `
* `if (use_cndv) call dgvs_vars%UpdateCNDVAccVars() `
* `if (crop_prog) call crop_vars%UpdateAccVars() `
* `call hist_update_hbuf() `
* `if (use_cndv): DO-SOME-STUFF`
* `! =========== changegroup E: ALM V1 (existing) ===========`
* `if ( use_ed )`
    * `if ( is_beg_curr_day() ) `
        * `call edmodel() `
        * `call clm_ed_link() `
        * `call setFilters() `
        * `call SurfaceAlbedo() `
* `! =========== changegroup E =========== `        
        
* `if (.not. use_noio) call hist_htapes_wrapup() `
* `if (.not. use_noio) call restFile_write() `

`end subroutine clm_drv`