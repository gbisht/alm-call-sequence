## ALM-v1: Directory hierarchy   

```
├── components/
    └── clm
        └──src
           ├── ED
           │   ├── biogeochem
           │   ├── biogeophys
           │   ├── fire
           │   └── main
           │
           ├── betr
           │   ├── betr_core
           │   ├── betr_math
           │   ├── betr_century
           │   └── betr_sminn
           │
           ├── biogeochem
           ├── biogeophys
           │   └── vsfm
           │
           │
           ├── cpl
           ├── dyn_subgrid
           ├── main
           │   ├── clm_pflotran_interfaceMod.F90
           │
           │
           └── utils
              └── clmfates_interfaceMod.F90

```


## ALM-v2: Proposed directory hierarchy   

```
├── components/
    └── clm
        └──src
           ├── external_model
           │   │
           │   ├── src
           │   │
           │   ├── ED    <---- Git submodule repo for ALM-v2.0
           │   │   ├── biogeochem
           │   │   ├── biogeophys
           │   │   ├── fire
           │   │   └── main
           │   │
           │   ├── betr    <---- Git submodule repo for ALM-v2.0
           │   │   ├── 3rd-party
           │   │   ├── cmake
           │   │   ├── contrib
           │   │   ├── example_input
           │   │   ├── regression-tests
           │   │   └── src
           │   │       │──Applications
           │   │       │──betr
           │   │       │──esmf_wrf_timemgr
           │   │       │──shr
           │   │       └──stub_clm
           │   │
           │   │── pflotran    <---- Git submodule repo for ALM-v2.0
           │   │   └── src
           │   │       ├── clm-pflotran
           │   │       └── pflotran
           │   │
           │   └── mpp    <---- Git submodule repo for ALM-v2.0
           │       ├── cmake
           │       ├── contrib
           │       ├── example_input
           │       └── src
           │           │──driver
           │           │   │──alm
           │           │   │──dummy
           │           │   └──standalone
           │           │──mpp
           │           └──tests
           │
           ├── biogeochem
           │
           ├── biogeophys
           │
           ├── cpl
           ├── dyn_subgrid
           ├── main
           │
           └── utils

```



## ALM-v2: Proposed interface to couple ALM and External Model (EM)


### Dependency graph


```
EMI: External Model Interface


                 --------------------------------> ALM: *StateType.F90 and *FluxType.F90
                /                                 /|\
               /                                   |
              /             ___                    |
             /             │   │-------------------|
            /              │ E │------------------------>  BeTR_Public_API_For_ALM
ALM Physics -------------> │ M │------------------------>  FATES_Public_API_For_ALM
                           │ I │------------------------>  PFLOTRAN_Public_API_For_ALM
                           │   │------------------------>  VSFM_Public_API_For_ALM
                            ---

```


### Pseudo Code

```
module ExternalModelInterfaceMod
  !------------------------------------------------------------------------------
  ! !DESCRIPTION:
  ! Interface to couple ALM and multiple external model
  !
  use ExternalModelInterfaceDataMod, only : external_model_interface_data
  !
  !
  implicit none
  private
  !
  !
  class(betr_simulation_alm_type), public, pointer :: em_betr
  type(hlm_fates_interface_type) , public, pointer :: em_fates
  type(pflotran_model_type)      , public, pointer :: em_pflotran
  type(mpp_vsfm_type)            , public, pointer :: em_vsfm

  integer :: num_em              ! Number of external models

  ! Index of the various external models in a simulation
  integer :: index_em_betr
  integer :: index_em_fates
  integer :: index_em_pflotran
  integer :: index_em_vsfm

  ! ID for various external models
  integer, public, parameter :: EM_ID_BeTR     = 1
  integer, public, parameter :: EM_ID_FATES    = 2
  integer, public, parameter :: EM_ID_PFLOTRAN = 3
  integer, public, parameter :: EM_ID_VSFM     = 4

  type(external_model_interface_data_list_type), pointer :: l2e_list(:)
  type(external_model_interface_data_list_type), pointer :: e2l_list(:)

  public :: EMI_Init
  public :: EMI_Init_EM
  public :: EMI_Driver
  
!-----------------------------------------------------------------------
  subroutine EMI_Init()
  !
  ! Determine the number of EMs are active
  !
  implicit none
  
  num_em            = 0  
  index_em_betr     = 0
  index_em_fates    = 0
  index_em_pflotran = 0
  index_em_vsfm     = 0

  nullify(epd)

  if (use_betr) then
     num_em = num_em + 1
     index_em_betr = num_em
     allocate(em_betr)
  endif

  if (use_ed) then
     num_em = num_em + 1
     index_em_fates = num_em
     allocate(em_fates)
  endif

  if (use_pflotran) then
     num_em = num_em + 1
     index_em_pflotran = num_em
     allocate(em_pflotran)
  endif

  if (use_vsfm) then
     num_em = num_em + 1
     index_em_vsfm = num_em
     allocate(em_vsfm)
  endif
  
  if (num_em == 0 ) return

  allocate(l2e_list(num_em))
  allocate(e2l_list(num_em))

  end subroutine EMI_Init()

!-----------------------------------------------------------------------

subroutine EMI_Init_EM(em_id)

  implicit none
  
  select case (em_id)
  case (EM_BeTR)
     call EMI_BeTR_Init(l2e_list(index_em_betr), e2l_list(index_em_betr))

  case (EM_FATES)
     call EMI_FATES_Init(l2e_list(index_em_fates), e2l_list(index_em_fates))

  case (EM_PFLOTRAN)
     call EMI_PFLOTRAN_Init(l2e_list(index_em_pflotran), e2l_list(index_em_pflotran))

  case (EM_VSFM)
     call EMI_VSFM_Init(l2e_list(index_em_vsfm), e2l_list(index_em_vsfm))

  case default
     call endrun('Unknown External Model')
  end select

  end subroutine EMI_Init_EM()

!-----------------------------------------------------------------------
  subroutine EMI_BeTR_Init(l2e, e2l)

  implicit none

  ! 1. Send beg[g/l/c/p] and end[g/l/c/p] information to allocate memory
  ! 2. Send additional information (e.g. parameters/initial condition/grid 
  !    information) to setup the model [THIS IS HALF-BAKED. NEEDS MORE WORK!!!]
  ! 3. Finally ask the external model to provide information about:
  !    - Fluxes/States it needs from ALM, and 
  !    - Fluxes/States it will return to ALM

  end subroutine EMI_BeTR_Init

!-----------------------------------------------------------------------
  subroutine EMI_FATES_Init(l2e, e2l)

  implicit none

  ! 1. Send beg[g/l/c/p] and end[g/l/c/p] information to allocate memory
  ! 2. Send additional information (e.g. parameters/initial condition/grid 
  !    information) to setup the model [THIS IS HALF-BAKED. NEEDS MORE WORK!!!]
  ! 3. Finally ask the external model to provide information about:
  !    - Fluxes/States it needs from ALM, and 
  !    - Fluxes/States it will return to ALM

  end subroutine EMI_FATES_Init

!-----------------------------------------------------------------------
  subroutine EMI_PFLOTRAN_Init(l2e, e2l)

  implicit none

  ! 1. Send beg[g/l/c/p] and end[g/l/c/p] information to allocate memory
  ! 2. Send additional information (e.g. parameters/initial condition/grid 
  !    information) to setup the model [THIS IS HALF-BAKED. NEEDS MORE WORK!!!]
  ! 3. Finally ask the external model to provide information about:
  !    - Fluxes/States it needs from ALM, and 
  !    - Fluxes/States it will return to ALM

  end subroutine EMI_PFLOTRAN_Init

!-----------------------------------------------------------------------
  subroutine EMI_VSFM_Init(l2e, e2l)

  implicit none

  ! 1. Send beg[g/l/c/p] and end[g/l/c/p] information to allocate memory
  ! 2. Send additional information (e.g. parameters/initial condition/grid 
  !    information) to setup the model [THIS IS HALF-BAKED. NEEDS MORE WORK!!!]
  ! 3. Finally ask the external model to provide information about:
  !    - Fluxes/States it needs from ALM, and 
  !    - Fluxes/States it will return to ALM

  end subroutine EMI_VSFM_Init
!-----------------------------------------------------------------------

  subroutine EMI_Driver(em_id, em_stage)

  implicit none
  
  integer, intent(in) :: em_id      ! Index of external model( EM_BeTR or EM_FATES or etc.)
  integer, intent(in) :: em_stage   ! An identifier when the external model is called multiple times.
                                    ! E.g. FATES is called to solve sun-shaded fraction, btran, 
                                    !      photosynthesis, etc. Thus, em_stage could be
                                    !      FATES_SUNFRAC, FATES_BTRAN, FATES_PHOTOSYNTHESIS.
                                    !

  select case (em_id)
  case (EM_ID_BeTR)


  select case (em_id)
  case (EM_BeTR)

     call EMID_Pack_Data_for_EM(emid(index_em_betr), em_stage)

     call em_betr%Driver(em_stage, &
                         l2e_list(index_em_fates), &
                         e2l_list(index_em_fates), &
                         err_status)

     call EMID_Unpack_Data_from_EM(emid(index_em_betr), em_stage)

  case (EM_FATES)

     call EMID_Pack_Data_for_EM(emid(index_em_fates), em_stage)

     call ep_fates%Driver(em_stage, &
                          l2e_list(index_em_fates), &
                          e2l_list(index_em_fates), &
                          err_status)

     call EMID_Unpack_Data_from_EM(emid(index_ep_fates), em_stage)

  case (EM_PFLOTRAN)
     call EMID_Pack_Data_for_EM(emid(index_ep_pflotran), em_stage)

     call ep_pflotran%Driver(em_stage, &
                             l2e_list(index_ep_pflotran), &
                             e2l_list(index_ep_pflotran), &
                             err_status)

     call EMID_Unpack_Data_from_EM(emid(index_ep_pflotran), em_stage)

  case (EM_VSFM)

     call EMID_Pack_Data_for_EM(emid(index_ep_vsfm), em_stage)

     call ep_vsfm%Driver(em_stage, &
                         l2e_list(index_ep_vsfm), &
                         e2l_list(index_ep_vsfm), &
                         err_status)

     call EMID_Unpack_Data_from_EM(emid(index_ep_vsfm), em_stage)


  case default
     call endrun('Unknown External Model')
  end select

  end subroutine EMI_Driver

```


```
module ExternalModelInterfaceDataMod

  type, public :: external_model_interface_data

     character (len=32)  :: name               ! Short name
     character (len=128) :: long_name          ! Long name of data
     character (len=24)  :: units              ! Units

     character (len=1)   :: avgflag            ! Needed for output to history file

     logical             :: is_int_type        ! Is data of an integer type?
     logical             :: is_real_type       ! Is data of real type?

     integer             :: num_em_stages      ! Number of EM stages in which the data is exchanged
     integer, pointer    :: em_stage_ids(:)    ! ID of EM stages in which the data is exchanged

     integer             :: ndim               ! Dimension of the data

     ! Dimension name of the data (e.g. 'column','levscpf', etc).
     character (len=10) :: dim1_name
     character (len=10) :: dim2_name           
     character (len=10) :: dim3_name
     character (len=10) :: dim4_name

     integer :: dim1_beg_clump, dim1_end_clump
     integer :: dim2_beg_clump, dim2_end_clump
     integer :: dim3_beg_clump, dim3_end_clump
     integer :: dim4_beg_clump, dim4_end_clump

     integer :: dim1_beg_proc, dim1_end_proc
     integer :: dim2_beg_proc, dim2_end_proc
     integer :: dim3_beg_proc, dim3_end_proc
     integer :: dim4_beg_proc, dim4_end_proc

     integer, pointer :: data_int_1d(:)
     integer, pointer :: data_int_2d(:,:)
     integer, pointer :: data_int_3d(:,:,:)

     real(r8), pointer :: data_real_1d(:)
     real(r8), pointer :: data_real_2d(:,:)
     real(r8), pointer :: data_real_3d(:,:,:)
     real(r8), pointer :: data_real_4d(:,:,:,:)

     type(external_model_interface_data), pointer:: next

  end type external_model_interface_data 

  type, public :: external_model_interface_data_list_type
     PetscInt                                        :: num_data
     type(external_model_interface_data), pointer    :: first
     type(external_model_interface_data), pointer    :: data(:)
  end type external_model_interface_data_list_type

end module ExternalModelInterfaceDataMod
```
